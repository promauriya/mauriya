import React from 'react';
import { Button} from "react-bootstrap";

export default class Footer extends React.Component {

    render() {
        return (
            <div className="align-content-center">
                <Button variant="primary" className="m-1">HTML5</Button>
                <Button variant="secondary" className="m-1">CSS3</Button>
                <Button variant="success" className="m-1">PHP 7.x</Button>
                <Button variant="warning" className="m-1">React</Button>
                <Button variant="danger" className="m-1">React-Native</Button>
                <Button variant="info" className="m-1">NPM</Button>
                <Button variant="light" className="m-1">MongoDB</Button>
                <Button variant="dark" className="m-1">Mysql</Button>
                <Button variant="primary" className="m-1">Javascript</Button>
                <Button variant="secondary" className="m-1">jQuery</Button>
                <Button variant="success" className="m-1">AWS Services</Button>
                <Button variant="warning" className="m-1">WordPress</Button>
                <Button variant="danger" className="m-1">SocialEngine (Zend Framework)</Button>
                <Button variant="info" className="m-1">Expo</Button>
                <Button variant="light" className="m-1">CMS</Button>
                <Button variant="dark" className="m-1">Drupal</Button>
                <Button variant="primary" className="m-1">Joomla</Button>
                <Button variant="secondary" className="m-1">Bigcommerce</Button>
                <Button variant="success" className="m-1">Ajax</Button>
                <Button variant="warning" className="m-1">Git</Button>
                <Button variant="danger" className="m-1">SVN</Button>
                <Button variant="info" className="m-1">PHPStorm</Button>
                <Button variant="light" className="m-1">WebStorm</Button>
                <Button variant="dark" className="m-1">Kanban</Button>
            </div>
        );
    }
}
