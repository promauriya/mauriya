import React from 'react';
import {Card} from "react-bootstrap";
import Pdf from '../Praveen.pdf';

export default class Praveen extends React.Component {

    render() {
        return (
            <div>
                <Card>
                    <Card.Body>
                        <Card.Title>Resume</Card.Title>
                        <Card.Subtitle className="mb-2 text-muted">Resume</Card.Subtitle>
                        <Card.Text>
                            <a href={Pdf} target="_blank" rel="noopener noreferrer">Resume Pdf</a>
                        </Card.Text>
                    </Card.Body>
                </Card>
            </div>
        );
    }
}
