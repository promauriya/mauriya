import React from 'react';
import {Card} from "react-bootstrap";

export default class Contact extends React.Component {

    render() {
        return (
            <div>
                <Card>
                    <Card.Body>
                        <Card.Title>Contact</Card.Title>
                        <Card.Subtitle className="mb-2 text-muted">Details</Card.Subtitle>
                        <Card.Text>
                            <strong>Praveen Mahato (Ryan)</strong><br/>
                            <strong>Email:</strong> promauriya@gmail.com<br/>
                            <strong>Skype:</strong> mauriya1<br/>
                        </Card.Text>
                    </Card.Body>
                </Card>
            </div>
        );
    }
}
