import React from 'react';
import {Card, Image} from "react-bootstrap";
import Mauriya from "../mauriya.png";

export default class Home extends React.Component {


    render() {
        return (
            <div>
                <Card>
                    <Card.Body>
                        <Card.Title>Home</Card.Title>
                        <Card.Subtitle className="mb-2 text-muted">My Experience, My Skills</Card.Subtitle>
                        <Card.Text>
                            <Image src={Mauriya} className="float-right p-1" roundedCircle />I am a Web Developer first in my life than another thing else. Because my hobby and passion is web development. I enjoy my life being a developer. I enjoy every bit of it. I start learning HTML before my high school.Praveen Mauriya I began helping my cousins to build websites early. I fill passionate to solve complex inter-script integration issues. It makes me happy. I started my career as a freelance developer when I was in last year of undergraduate IT studies building Joomla components involving PHP, Javascript, and MySQL. I have gained my skills solving problems with different scripts and CMS.<br/><br/>I use my skills and idea to solve inter-script and data processing problems which most of the peoples don't want to take on. I have connected PHP scripts, CMS, Javascript application and C++ application with one another for login synchronization and data communication. I have developed REST API for data communication and connection with the time frame token system which works seamlessly with any script application. I am a team as well as an individual player. I am a good listener and excellent analyzer. So, I perform well as a developer, taking care of quality, requirement, and performance in given time. I am open to new technology and always learning.<br/><br/>
                        </Card.Text>
                        <Card.Link href="https://www.linkedin.com/in/ryan-mahato-a605ab143/" target="_blank" rel="noopener noreferrer">Linkedin</Card.Link>
                        <Card.Link href="https://wordpress.org/plugins/create-restaurant-menu/" target="_blank" rel="noopener noreferrer">WP Free Plugin</Card.Link>
                        <Card.Link href="https://www.codester.com/items/23285/restaurant-menu-cart-payment-wordpress" target="_blank" rel="noopener noreferrer">WP Premium Plugin</Card.Link>
                        <Card.Link href="http://4columntemplate.blogspot.com/" rel="noopener noreferrer">Blogspot</Card.Link>
                        <Card.Link href="https://gitlab.com/promauriya" target="_blank" rel="noopener noreferrer">Git Repo</Card.Link>
                        <Card.Link href="/Praveen">Resume</Card.Link>
                    </Card.Body>
                </Card>
            </div>
        );
    }
}
