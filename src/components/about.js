import React from 'react';
import {Card} from "react-bootstrap";

export default class About extends React.Component {

    render() {
        return (
            <div>
                <Card>
                    <Card.Body>
                        <Card.Title>About</Card.Title>
                        <Card.Subtitle className="mb-2 text-muted">Website Professional Portfolio</Card.Subtitle>
                        <Card.Text>
                            <strong>Name:</strong> Praveen Mahato<br/>
                            <strong>Education:</strong> B.Tech in IT<br/>
                            <strong>Languages & Scripts:</strong> HTML5, CSS3, PHP, Javascript, MySQL DB, jQuery, Ajax, NodeJS, React, MongoDB, Other Frameworks, etc...<br/>
                            <strong>Software's:</strong> Git, SVN, PHPStorm, WebStorm, Jira, Kanban, Mysql Workbench, FileZilla, Putty, BackTrack, etc...<br/>
                            <strong>CMS:</strong> Wordpress, Joomla, Bigcommerce, Drupal, Magento, etc...<br/>
                            <strong>Operating Systems:</strong> Windows, OS X 10.x, Linux, etc...<br/>
                            <strong>Training & Certification:</strong> Amazon Riches on FBA stores fulfillment by Amazon, Microsoft Windows Server 2008 (70-640) Certified, HTML 5, CSS3, Website designing,  PHP, ajax, MySql, C, C++, Java scripting, CCNA, Microsoft Windows Server 2008 (70-646, 70-642), etc...<br/>
                            <strong>Summary Career:</strong> Senior Web Developer(Mar/2017- Present): Webmaster (Jan/2016-Dec/2016): Freelancer (Feb/2015-Jan/2016): Senior Developer (Jan/2013-Dec/2014): Designer and Programmer (Aug/2009-Jan/2013): Network Engineer (Feb/2009-Aug/2009)
                        </Card.Text>
                        <Card.Link href="/Praveen">Latest Resume</Card.Link>
                    </Card.Body>
                </Card>
            </div>
        );
    }
}
