import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { Container, Row, Col } from 'react-bootstrap';
import Home from './components/home';
import About from './components/about';
import Contact from './components/contact';
import Praveen from './components/praveen';
import Footer from './components/footer';
import Navi from './components/navi';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import Videourl from './BeachSea.mp4';

export default class App extends React.Component {

  render() {
    return (
        <Router>
            <video id="background-video" loop autoPlay>
                <source src={Videourl} type="video/mp4" />
                <source src={Videourl} type="video/ogg" />
                Your browser does not support the video tag.
            </video>
            <Container id="desktop">
                <Row className="justify-content-around py-4">
                    <Col xs xl="10">
                        <Navi />
                    </Col>
                </Row>
                <Row className="justify-content-around py-3">
                    <Col xs xl="10">
                        <Switch>
                            <Route path="/Praveen">
                                <Praveen />
                            </Route>
                            <Route path="/about">
                                <About />
                            </Route>
                            <Route path="/contact">
                                <Contact />
                            </Route>
                            <Route path="/">
                                <Home />
                            </Route>
                        </Switch>
                    </Col>
                </Row>
                <Row className="justify-content-around py-3">
                    <Col xs xl="10">
                        <Footer />
                    </Col>
                </Row>
            </Container>
        </Router>
    );
  }
}
